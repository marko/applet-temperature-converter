package nl.utwente.marko.tempconvert;

public class Converter {

    private static final double ratio = 1.8000;
    private static final double diff = 32.00;

    public double celsiusToFahrenheit(double celsius) {
        return celsius * ratio + diff;
    }

    public double fahrenheitToCelsius(double fahrenheit) {
        return (fahrenheit - diff) / ratio;
    }
}
