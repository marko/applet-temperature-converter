package nl.utwente.marko.tempconvert;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class TempConvert extends HttpServlet {

    private final static String title = "Celsius to Fahrenheit Translation";
    private final static String docType = "<!DOCTYPE HTML>\n";
    private final static String header =
            docType +
            "<HTML>\n" +
            "<HEAD><TITLE>" + title + "</TITLE>" +
            "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
            "</HEAD>\n";

    private Converter converter;

    public void init() {
        converter = new Converter();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        var reqParameter = request.getParameter("value");
        try {
            var result = converter.celsiusToFahrenheit(Double.parseDouble(reqParameter));
            ok(response, reqParameter, result);
        } catch (NumberFormatException e) {
            err(response, reqParameter);
        }


    }

    private void ok(HttpServletResponse response, String reqParameter, double result) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String title = "Celsius to Fahrenheit Translation";
        out.println(header +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius value: " +
                reqParameter + "\n" +
                "  <P>Fahrenheit value: " +
                result +
                "</BODY></HTML>");
    }

    private void err(HttpServletResponse response, String reqParameter) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println(header +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Your input (" +
                reqParameter + ") " +
                " is not a valid value " +
                "</BODY></HTML>");
    }


}
