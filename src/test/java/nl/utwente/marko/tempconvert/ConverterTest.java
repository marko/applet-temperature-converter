package nl.utwente.marko.tempconvert;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConverterTest {

    private final double ABSOLUTE_ZERO_C = -273.15;
    private final double ABSOLUTE_ZERO_F = -459.67;
    private final double PARITY_C = -40.00;
    private final double PARITY_F = -40.00;
    private final double FREEZING_POINT_C = 0.00;
    private final double FREEZING_POINT_F = 32.00;
    private final double BODY_TEMPERATURE_C = 37.00;
    private final double BODY_TEMPERATURE_F = 98.6;
    private final double BOILING_POINT_C = 100.00;
    private final double BOILING_POINT_F = 212.00;

    private final double DELTA = 0.00001;


    private final Converter converter = new Converter();

    @Test
    void testCelsiusToFahrenheit() {
        assertEquals(converter.celsiusToFahrenheit(ABSOLUTE_ZERO_C), ABSOLUTE_ZERO_F, DELTA);
        assertEquals(converter.celsiusToFahrenheit(PARITY_C), PARITY_F, DELTA);
        assertEquals(converter.celsiusToFahrenheit(FREEZING_POINT_C), FREEZING_POINT_F, DELTA);
        assertEquals(converter.celsiusToFahrenheit(BODY_TEMPERATURE_C), BODY_TEMPERATURE_F, DELTA);
        assertEquals(converter.celsiusToFahrenheit(BOILING_POINT_C), BOILING_POINT_F, DELTA);
    }

    @Test
    void testFahrenheitToCelsius() {
        assertEquals(converter.fahrenheitToCelsius(ABSOLUTE_ZERO_F), ABSOLUTE_ZERO_C, DELTA);
        assertEquals(converter.fahrenheitToCelsius(PARITY_F), PARITY_C, DELTA);
        assertEquals(converter.fahrenheitToCelsius(FREEZING_POINT_F), FREEZING_POINT_C, DELTA);
        assertEquals(converter.fahrenheitToCelsius(BODY_TEMPERATURE_F), BODY_TEMPERATURE_C, DELTA);
        assertEquals(converter.fahrenheitToCelsius(BOILING_POINT_F), BOILING_POINT_C, DELTA);
    }
}
